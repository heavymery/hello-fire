import { HelloFirePage } from './app.po';

describe('hello-fire App', () => {
  let page: HelloFirePage;

  beforeEach(() => {
    page = new HelloFirePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
